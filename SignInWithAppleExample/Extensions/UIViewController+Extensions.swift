//
//  UIViewController+Extensions.swift
//  SignInWithAppleExample
//
//  Created by Anastasia Markovets on 11/25/20.
//

import UIKit

extension UIViewController {
    func setupDismissKeyboardOnTap() {
        let tap = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    func showErrorAlert() {
        let alet = UIAlertController(title: "Error", message: "Something went wrong.", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alet.addAction(cancelAction)
        present(alet, animated: true, completion: nil)
    }
}
