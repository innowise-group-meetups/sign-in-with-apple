//
//  AppDelegate.swift
//  SignInWithAppleExample
//
//  Created by Anastasia Markovets on 11/25/20.
//

import UIKit
import Firebase
import AuthenticationServices

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        if Auth.auth().currentUser != nil {
            window?.rootViewController = WelcomeViewController()
        } else {
            window?.rootViewController = SignInViewController()
        }
        checkIsUserAuthorizedWithAppleOnDevice()
        return true
    }

    private func checkIsUserAuthorizedWithAppleOnDevice() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        appleIDProvider.getCredentialState(forUserID: UserDefaults.standard.string(forKey: "userId") ?? "") {  (credentialState, error) in
            switch credentialState {
            case .authorized:
                print("The Apple ID credential is valid.")
                break
            case .revoked:
                print("The Apple ID credential is revoked.")
                break
            case .notFound:
                print("No credential was found, so show the sign-in UI.")
            default:
                break
            }
        }
    }
}
