//
//  WelcomeViewController.swift
//  SignInWithAppleExample
//
//  Created by Anastasia Markovets on 11/25/20.
//

import UIKit
import SnapKit
import Firebase

class WelcomeViewController: UIViewController {
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Welcome!" + "\n" + "You are successfully logged in."
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()

    private let emailLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .systemBlue
        return label
    }()

    private let fullNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .systemGreen
        return label
    }()

    private let signOutButton: UIButton = {
        let button = UIButton()
        button.setTitle("Sign Out", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .systemBlue
        button.layer.cornerRadius = 15
        button.addTarget(self, action: #selector(signOut), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        addSubviews()
        setupConstraints()

        emailLabel.text = UserDefaults.standard.string(forKey: "userEmail") ?? ""
        fullNameLabel.text = UserDefaults.standard.string(forKey: "userFullName") ?? ""
    }

    private func addSubviews() {
        view.addSubview(titleLabel)
        view.addSubview(emailLabel)
        view.addSubview(fullNameLabel)
        view.addSubview(signOutButton)
    }

    private func setupConstraints() {
        titleLabel.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(30)
            $0.centerY.equalToSuperview()
        }
        emailLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(20)
            $0.leading.trailing.equalTo(titleLabel)
        }
        fullNameLabel.snp.makeConstraints {
            $0.top.equalTo(emailLabel.snp.bottom).offset(20)
            $0.leading.trailing.equalTo(titleLabel)
        }
        signOutButton.snp.makeConstraints {
            $0.height.equalTo(50)
            $0.leading.trailing.bottom.equalTo(view.safeAreaLayoutGuide).inset(20)
        }
    }

    @objc private func signOut() {
        do {
            try Auth.auth().signOut()
            UserDefaults.standard.removeObject(forKey: "userEmail")
            UserDefaults.standard.removeObject(forKey: "userFullName")
            dismiss(animated: true, completion: nil)
        } catch {
            showErrorAlert()
        }
    }
}
